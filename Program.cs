using System;
using analogturtle.Core;

namespace analogturtle
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            (new MainWindow ()).Run ();
        }
    }
}
