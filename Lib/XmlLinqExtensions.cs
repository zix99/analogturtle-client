using System;
using System.Xml.Linq;
using System.ComponentModel;

namespace analogturtle.Lib
{
    public static class XmlLinqExtensions
    {
        public static T GetAttribute<T>(this XElement ele, string name, T def = default(T))
        {
            var attr = ele.Attribute(name);
            if (attr == null)
                return def;

            var converter = TypeDescriptor.GetConverter (typeof(T));
            return (T)converter.ConvertFromString (attr.Value);
        }
    }
}

