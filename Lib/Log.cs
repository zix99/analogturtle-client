using System;
using System.IO;

namespace analogturtle.Lib
{
	public static class Log
	{
		private static readonly StreamWriter outFile;
		private static object _lock = new object ();
        private static readonly DateTime _start = DateTime.Now;

		static Log()
		{
			outFile = new StreamWriter(File.OpenWrite("analogsheep.log"));
            Write ("Logger started");
		}

        public static void LogInfo(string msg, params object[] args)
        {
            Write ("I", msg, args);
        }

        public static void LogInfo(string msg)
        {
            Write(msg, "I");
        }

        public static void LogErr(string msg, params object[] args)
        {
            Write ("E", msg, args);
        }

        public static void LogErr(string msg)
        {
            Write(msg, "E");
        }

        public static void LogWarn(string msg, params object[] args)
        {
            Write ("W", msg, args);
        }

        public static void LogWarn(string msg)
        {
            Write(msg, "W");
        }

        private static void Write(string level, string msg, params object[] args)
        {
            Write(string.Format(msg, args), level);
        }

		private static void Write(string msg, string level = "I")
		{
            string full = string.Format ("{0,1:000.0} [{1}]: {2}", (DateTime.Now - _start).TotalSeconds, level, msg);

			lock (_lock)
			{
				Console.WriteLine (full);
				outFile.WriteLine (full);
			}
		}
	}
}

