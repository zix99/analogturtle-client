using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using Imaging = System.Drawing.Imaging;
using System.IO;
using analogturtle.Lib;
using analogturtle.Core.Rendering;
using analogturtle.Core.Rendering.Layers;

namespace analogturtle.Core
{
	public class MainWindow : GameWindow
	{
        private static readonly GraphicsMode GRAPHICS_MODE = new GraphicsMode (new ColorFormat (32), 0, 0, 16);

        private Shell _shell;

		public MainWindow ()
            :base(1440, 720, GRAPHICS_MODE, "Analog Turtle")
		{
		}

        protected override void OnLoad(EventArgs e)
        {
            this.VSync = VSyncMode.On;
            this.TargetRenderFrequency = 60.0;
            this.TargetUpdateFrequency = 0;

            GL.ClearColor (1f, 0f, 0f, 0f);

            Log.LogInfo ("Reading shader...");
            _shell = Shell.Load ("shells/text.shell");
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (_shell != null)
            {
                _shell.Dispose();
            }

            #if DEBUG
            GC.Collect();
            GC.WaitForPendingFinalizers();
            #endif

            base.OnClosing(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize (e);

            GL.Viewport (0, 0, this.Width, this.Height);

            var projection = Matrix4.CreateOrthographicOffCenter (0f, 1f, 1f, 0f, 0f, 1f);
            GL.MatrixMode (MatrixMode.Projection);
            GL.LoadMatrix (ref projection);
        }

        protected override void OnKeyDown(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.OnKeyDown (e);
            if (e.Key == Key.Escape)
            {
                this.Exit ();
            }
            else if(e.Key == Key.F12)
            {
                this.TakeScreenshot ();
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.MatrixMode (MatrixMode.Modelview);
            GL.LoadIdentity ();

            if (_shell != null)
            {
                _shell.Render ();
            }
            this.SwapBuffers ();

#if DEBUG
            var err = GL.GetError ();
            if (err != ErrorCode.NoError)
            {
                Log.LogErr ("OpenGL had error: {0}", err.ToString ());
            }
#endif
        }

        protected void TakeScreenshot()
        {
            try
            {
                using (var bmp = new Bitmap(this.Width, this.Height))
                {
                    var bits = bmp.LockBits(
                        new Rectangle(0,0,bmp.Width,bmp.Height),
                        Imaging.ImageLockMode.WriteOnly,
                        Imaging.PixelFormat.Format24bppRgb);

                    GL.ReadPixels(0, 0, this.Width, this.Height, PixelFormat.Bgr, PixelType.UnsignedByte, bits.Scan0);

                    bmp.UnlockBits(bits);
                    bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    if (!Directory.Exists("screenshots"))
                        Directory.CreateDirectory("screenshots");

                    string filename = string.Format("screenshots/{0}.png", DateTime.Now.ToString("yyyy-MM-dd_HHmmss.fff"));
                    bmp.Save(filename, Imaging.ImageFormat.Png);
                    Log.LogInfo("Screenshot saved to '{0}'", filename);
                }
            }
            catch
            {
                Log.LogErr ("Failed to save screenshot");
            }

        }
	}
}

