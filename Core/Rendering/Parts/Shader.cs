using System;
using OpenTK.Graphics.OpenGL;

namespace analogturtle.Core.Rendering.Parts
{
    public class Shader
    {
        private Shader()
        {
        }

        private static Version _glslVersion;
        public static Version Version
        {
            get
            {
                if (_glslVersion == null)
                {
                    _glslVersion = new Version( GL.GetString(StringName.ShadingLanguageVersion).Split(' ')[0] );
                }
                return _glslVersion;
            }
        }
    }
}

