using System;
using OpenTK.Graphics.OpenGL;
using analogturtle.Lib;
using System.Drawing;
using System.Drawing.Imaging;

namespace analogturtle.Core.Rendering.Parts
{
    public class Texture : ICloneable, IDisposable
    {
        private readonly bool _cloned;
        private readonly int _id;

        public Texture ()
        {
            _id = GL.GenTexture ();
            SetFilter (TextureMinFilter.Linear, TextureMagFilter.Linear);
        }

        protected Texture(int id)
        {
            _id = id;
            _cloned = true;
        }

        public static Texture Load(Bitmap bitmap)
        {
            using (var flipped = (Bitmap)bitmap.Clone())
            {
                flipped.RotateFlip(RotateFlipType.RotateNoneFlipY);
                var tex = new Texture();

                BitmapData bits = null;
                try
                {
                    bits = flipped.LockBits(new Rectangle(0, 0, flipped.Width, flipped.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                    tex.Bind();
                    GL.TexImage2D(
                        TextureTarget.Texture2D,
                        0,
                        PixelInternalFormat.Rgba,
                        flipped.Width,
                        flipped.Height,
                        0,
                        OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                        PixelType.UnsignedByte,
                        bits.Scan0);

                }
                finally
                {
                    if (bits != null)
                        flipped.UnlockBits(bits);
                }

                return tex;
            }
        }

        ~Texture()
        {
            Log.LogWarn ("Texture undisposed");
        }

        private bool _disposed;
        public void Dispose()
        {
            if (!_disposed)
            {
                if (!_cloned)
                    GL.DeleteTexture (_id);
                _disposed = true;
                GC.SuppressFinalize (this);
            }
        }

        public void Bind()
        {
            GL.BindTexture (TextureTarget.Texture2D, _id);
        }

        public void Unbind()
        {
            GL.BindTexture (TextureTarget.Texture2D, 0);
        }

        public void SetFilter(TextureMinFilter min, TextureMagFilter mag)
        {
            Bind ();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)min);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)mag);
        }

        public void SetWrap(TextureWrapMode mode)
        {
            Bind ();
            GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)mode);
            GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)mode);
        }

        public Texture Clone()
        {
            return new Texture (_id);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}

