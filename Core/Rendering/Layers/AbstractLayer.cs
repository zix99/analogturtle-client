using System;
using OpenTK.Graphics.OpenGL;
using System.Xml.Linq;
using analogturtle.Lib;

namespace analogturtle.Core.Rendering.Layers
{
    public abstract class AbstractLayer : IComponent
    {
        private static readonly float[] TEX_COORDS = {
            0f, 1f,
            1f, 1f,
            1f, 0f,
            0f, 0f
        };

        private static readonly float[] VERTS = {
            0f, 0f,
            1f, 0f,
            1f, 1f,
            0f, 1f
        };

        private readonly bool _blend = false;
        private readonly BlendingFactorSrc _blendSource = BlendingFactorSrc.One;
        private readonly BlendingFactorDest _blendDestination = BlendingFactorDest.One;

        public AbstractLayer (XElement root)
        {
            var blend = root.Element("blend");
            if (blend != null)
            {
                _blend = true;
                _blendSource = blend.GetAttribute<BlendingFactorSrc>("src", BlendingFactorSrc.One);
                _blendDestination = blend.GetAttribute<BlendingFactorDest>("dest", BlendingFactorDest.One);
            }
        }

        public virtual void Dispose()
        {

        }

        public virtual void Render(double time)
        {
            GL.EnableClientState (ArrayCap.VertexArray);
            GL.EnableClientState (ArrayCap.TextureCoordArray);

            if (_blend)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(_blendSource, _blendDestination);
            }

            GL.TexCoordPointer (2, TexCoordPointerType.Float, 0, TEX_COORDS);
            GL.VertexPointer (2, VertexPointerType.Float, 0, VERTS);
            GL.DrawArrays (PrimitiveType.Quads, 0, 4);

            if (_blend)
            {
                GL.Disable(EnableCap.Blend);
            }

            GL.DisableClientState (ArrayCap.VertexArray);
            GL.DisableClientState (ArrayCap.TextureCoordArray);
        }

    }
}

