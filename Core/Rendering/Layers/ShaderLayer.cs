using System;
using OpenTK.Graphics.OpenGL;
using analogturtle.Lib;
using OpenTK;
using System.Xml.Linq;
using System.Collections.Generic;
using analogturtle.Core.Rendering.Parts;

namespace analogturtle.Core.Rendering.Layers
{
    public class ShaderLayer : AbstractLayer, IDisposable
    {
        private readonly int _programId;
        private readonly int _shaderId;
        private readonly int _timeUniform;

        private class ShaderTexture
        {
            public string Name = null;
            public int UniformId = 0;
            public Texture Tex = null;
        }
        private readonly List<ShaderTexture> _textures = new List<ShaderTexture>();

        private ShaderLayer (XElement root, ShellContent content)
            :base(root)
        {
            _programId = GL.CreateProgram ();
            _shaderId = GL.CreateShader (ShaderType.FragmentShader);

            string source = root.Element("source").Value;

            GL.ShaderSource (_shaderId, root.Element("source").Value);
            GL.CompileShader (_shaderId);
            GL.AttachShader (_programId, _shaderId);

            string shaderLog = GL.GetShaderInfoLog (_shaderId);
            if (!string.IsNullOrEmpty(shaderLog))
            {
                Log.LogWarn ("Shader had log:\n" + shaderLog);
            }

            GL.LinkProgram (_programId);
            string programLog = GL.GetProgramInfoLog (_programId);
            if (!string.IsNullOrEmpty(programLog))
            {
                Log.LogWarn ("Program had log:\n" + programLog);
            }

            //Global uniforms
            _timeUniform = GL.GetUniformLocation (_programId, "time");

            //Textures
            foreach(var tex in root.Elements("texture"))
            {
                string name = tex.GetAttribute<string>("name", "undef");
                var texture = content.GetOrCreateTexture(tex.GetAttribute<string>("src"));
                if (texture != null && !string.IsNullOrEmpty(name))
                {
                    _textures.Add(new ShaderTexture
                    {
                        Name = name,
                        UniformId = GL.GetUniformLocation(_programId, name),
                        Tex = texture
                    });
                }
            }
        }

        ~ShaderLayer()
        {
            Log.LogWarn ("Undisposed Layer");
        }

        public static ShaderLayer Load(XElement root, ShellContent content)
        {
            return new ShaderLayer (root, content);
        }

        private bool _disposed;
        public override void Dispose()
        {
            if (!_disposed)
            {
                GL.DetachShader (_programId, _shaderId);
                GL.DeleteShader (_shaderId);
                GL.DeleteProgram (_programId);

                GC.SuppressFinalize (this);
                _disposed = true;
            }

            base.Dispose();
        }

        public override void Render(double time)
        {
            GL.UseProgram (this._programId);
            GL.Uniform1 (_timeUniform, (float)time);

            for (int i=0; i<_textures.Count; ++i)
            {
                GL.ActiveTexture(TextureUnit.Texture0 + i);
                _textures[i].Tex.Bind();
                GL.Uniform1(_textures[i].UniformId, i);
            }

            base.Render (time);

            for (int i=_textures.Count-1; i >= 0; --i)
            {
                GL.ActiveTexture(TextureUnit.Texture0 + i);
                _textures[i].Tex.Unbind();
            }

            GL.UseProgram (0);
        }
    }
}

