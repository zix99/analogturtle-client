using System;
using System.Xml.Linq;
using analogturtle.Lib;

namespace analogturtle.Core.Rendering.Layers
{
    public static class LayerFactory
    {
        public static AbstractLayer LoadLayer(XElement root, ShellContent content)
        {
            string type = root.GetAttribute<string>("type").ToLower();
            switch(type)
            {
                case "shader":
                    return ShaderLayer.Load(root, content);
            }
            Log.LogWarn("Unknown layer type: {0}", type);
            return null;
        }
    }
}

