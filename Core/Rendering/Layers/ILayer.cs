using System;

namespace analogturtle.Core.Rendering.Layers
{
    public interface ILayer
    {
        void Render();
    }
}

