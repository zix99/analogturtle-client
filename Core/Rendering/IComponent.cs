using System;

namespace analogturtle.Core.Rendering
{
    public interface IComponent : IDisposable
    {
        void Render(double time);
    }
}

