using System;
using System.Xml.Linq;
using analogturtle.Core.Rendering.Layers;
using analogturtle.Core.Rendering.Components;
using analogturtle.Lib;

namespace analogturtle.Core.Rendering
{
    public static class ComponentFactory
    {
        public static IComponent LoadComponent(XElement component, ShellContent content)
        {
            switch(component.Name.LocalName.ToLower())
            {
                case "layer":
                    return LayerFactory.LoadLayer(component, content);
                case "clear":
                    return new ClearComponent(component);
                case "capture":
                    return new CaptureComponent(component, content);
            }
            Log.LogWarn("Unrecognized render component: {0}", component.Name);
            return null;
        }
    }
}

