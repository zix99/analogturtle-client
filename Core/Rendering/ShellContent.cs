using System;
using System.Collections.Generic;
using analogturtle.Core.Rendering.Parts;
using System.Xml.Linq;
using analogturtle.Lib;
using System.IO;
using System.Drawing;

namespace analogturtle.Core.Rendering
{
    public class ShellContent : IDisposable
    {
        private readonly Dictionary<string, Texture> _textures = new Dictionary<string, Texture>(StringComparer.OrdinalIgnoreCase);

        public ShellContent()
        {
        }

        private bool _disposed;
        public void Dispose()
        {
            if (!_disposed)
            {
                foreach(var tex in _textures)
                {
                    tex.Value.Dispose();
                }

                _disposed = true;
            }
        }

        public void ParseXml(XElement root)
        {
            foreach (var item in root.Elements())
            {
                switch (item.Name.LocalName.ToLower())
                {
                    case "texture":
                        LoadTexture(item);
                        break;
                }
            }
        }

        public Texture GetTexture(string key)
        {
            Texture texture;
            if (_textures.TryGetValue(key, out texture))
            {
                return texture;
            }
            return null;
        }

        public Texture GetOrCreateTexture(string key)
        {
            Texture texture = null;
            if (!_textures.TryGetValue(key, out texture))
            {
                _textures[key] = texture = new Texture();
            }
            return texture;
        }

        private void LoadTexture(XElement root)
        {
            string encoding = root.GetAttribute<string>("encoding", "base64").ToLower();
            string id = root.GetAttribute<string>("id", "<undef>");

            if (encoding == "base64")
            {
                using (var ms = new MemoryStream(Convert.FromBase64String(root.Value)))
                using (var bmp = new Bitmap(ms))
                {
                    _textures[id] = Texture.Load(bmp);
                }
            }
            else if(encoding == "none")
            {
                _textures[id] = new Texture();
            }
            else
            {
                Log.LogWarn("Unrecognized encoding for texture: {0}", encoding);
            }
        }
    }
}

