using System;
using System.Xml.Linq;
using analogturtle.Lib;
using OpenTK.Graphics.OpenGL;
using analogturtle.Core.Rendering.Parts;

namespace analogturtle.Core.Rendering.Components
{
    public class CaptureComponent : IComponent
    {
        private readonly Texture _texture;

        public CaptureComponent(XElement root, ShellContent content)
        {
            string id = root.GetAttribute<string>("dest");
            _texture = content.GetOrCreateTexture(id);

            if (_texture == null)
                throw new InvalidOperationException("Texture not found");
        }


        #region IComponent implementation
        public void Render(double time)
        {
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            _texture.Bind();
            GL.CopyTexImage2D(
                TextureTarget.Texture2D,
                0,
                PixelInternalFormat.Rgba,
                viewport[0],
                viewport[1],
                viewport[2],
                viewport[3],
                0);
        }
        #endregion
        #region IDisposable implementation
        public void Dispose()
        {

        }
        #endregion
    }
}

