using System;
using analogturtle.Lib;
using System.Linq;
using System.Xml.Linq;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace analogturtle.Core.Rendering.Components
{
    public class ClearComponent : IComponent
    {
        private readonly Color4 _color;

        public ClearComponent(XElement root)
        {
            float[] parts = root.GetAttribute<string>("color").Split(',').Select(x => float.Parse(x.Trim())).ToArray();
            _color = new Color4(parts[0], parts[1], parts[2], parts[3]);
        }

        #region IComponent implementation

        public void Render(double time)
        {
            GL.ClearColor(_color);
            GL.Clear(ClearBufferMask.ColorBufferBit);
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {

        }

        #endregion
    }
}

