using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using analogturtle.Lib;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Drawing;
using analogturtle.Core.Rendering.Parts;
using analogturtle.Core.Rendering.Layers;
using analogturtle.Core.Rendering.Components;
using System.Text;

namespace analogturtle.Core.Rendering
{
    public class Shell : IDisposable
    {
        public string Author{get; private set;}
        public string Title{get; private set;}
        public string Version{get; private set;}

        public double RunLength{ get; private set; }
        public double StartTime{ get; private set;}

        private double _time = 0.0;

        private readonly ShellContent _content = new ShellContent();
        private readonly List<IComponent> _components = new List<IComponent>();

        protected Shell ()
        {
        }

        private bool _disposed;
        public void Dispose()
        {
            if (!_disposed)
            {
                foreach(var component in _components)
                {
                    component.Dispose();
                }

                _content.Dispose();
                _disposed = true;
            }
        }

        public static Shell LoadXml(XElement root)
        {
            var shell = new Shell ();
            shell.Parse (root);
            return shell;
        }

        public static Shell LoadXml(Stream stream)
        {
            return LoadXml (XElement.Load(stream));
        }

        public static Shell LoadXml(string xml)
        {
            using(var ms = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                return LoadXml(ms);
            }
        }

        public static Shell Load(string filename)
        {
            using(var stream = File.OpenRead(filename))
            {
                return LoadXml (stream);
            }
        }

        protected void Parse(XElement root)
        {
            this.Author = root.GetAttribute<string>("author");
            this.Title = root.GetAttribute<string> ("title");
            this.Version = root.GetAttribute<string> ("version");

            //Meta
            var meta = root.Element("meta");
            if (meta != null)
            {
                var glsl = meta.Element("glsl");
                if (glsl != null)
                {
                    var required = new Version(glsl.GetAttribute("require", "1.0"));
                    if (required > Shader.Version)
                        throw new InvalidOperationException("Required shader version higher than available");
                }

                var runtime = meta.Element("runtime");
                if (runtime != null)
                {
                    this.RunLength = runtime.GetAttribute<double>("length", 30);
                    this.StartTime = this._time = runtime.GetAttribute<double>("start", 0);
                }
            }

            //Content
            foreach(var content in root.Elements("content"))
            {
                _content.ParseXml(content);
            }

            //Components!
            foreach(var component in root.Element("render").Elements())
            {
                try
                {
                    var renderer = ComponentFactory.LoadComponent(component, _content);
                    if (renderer != null)
                        _components.Add(renderer);
                }
                catch(Exception e)
                {
                    Log.LogErr("Failed to load component: {0}\n{1}", component.Name, e);
                }
            }
        }



        public void Render()
        {
            for (int i=0; i<_components.Count; ++i)
            {
                _components[i].Render(_time);
            }

            _time += (1 / 60.0);
        }
    }
}

